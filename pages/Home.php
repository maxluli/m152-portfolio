<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/monCss.css">
    <link rel="stylesheet" href="../css/uikit.min.css" />
    <script src="../js/uikit.min.js"></script>
    <script src="../js/uikit-icons.min.js"></script>
    <script src="https://kit.fontawesome.com/719158633b.js" crossorigin="anonymous"></script>
    <title>Home</title>
</head>

<body class="uk-width-1-1 uk-height-1-1">

    <nav style="background:skyblue" class="uk-navbar-container uk-margin-large-bottom" uk-navbar>

        <div class="uk-navbar-left">

            <ul class="uk-navbar-nav">
                <li class="uk-active"><a href="#">
                        <h1><i class="fas fa-burn uk-margin-top"></h1></i>
                    </a></li>
                <form action="" class="uk-margin-top">
                    <input class="uk-input uk-form-width-small" type="text" placeholder="Input">
                    <button class="uk-button uk-button-default"><i class="fas fa-search"></i></button>
                </form>
            </ul>

        </div>

        <div class="uk-navbar-right">

            <ul class="uk-navbar-nav">
                <li><a href="Post.php">Post &nbsp<i class="fas fa-plus"></i></a></li>
                <li><a href="#">Home &nbsp<i class="fas fa-home"></i></a></li>
            </ul>

        </div>

    </nav>

    <div class="uk-grid-small uk-width-1-1 uk-child-width-expand@s uk-text-center" uk-grid="masonry: true">

        <div class="uk-grid-small uk-child-width-1-1 uk-text-center uk-margin-small-left" uk-grid="masonry: true">
            <div>
                <div class="uk-card uk-card-default ">
                    <div class="uk-card-media-top">
                        <img src="images/light.jpg" alt="">
                    </div>
                    <div class="uk-card-body">
                        <h3 class="uk-card-title"><img src="../Img/Blog.jpg"></h3>
                        <h2>NOM DE VOTRE BLOG</h2>
                        <p>
                            45 followers,13 Posts
                        </p>
                        <img data-src="../Img/PPplaceholder.png" width="100px" height="100px" alt="" uk-img>
                    </div>
                </div>
                <div>
                    <div class="uk-card uk-card-primary uk-card-body uk-margin-top">
                        <h3 class="uk-card-title">Amis</h3>
                        <p>Michel_P</p>
                        <p>Michael_T</p>
                        <p>Lisa_R</p>
                        <p>Philipe_p</p>
                    </div>
                </div>
                <div>
                    <div class="uk-card uk-card-secondary uk-card-body uk-margin-top">
                        <h3 class="uk-card-title">Amis</h3>
                        <p>Michel_P</p>
                        <p>Michael_T</p>
                        <p>Lisa_R</p>
                        <p>Philipe_p</p>
                    </div>
                </div>


            </div>
        </div>

        <div class="uk-grid-small  uk-child-width-1-1 uk-text-center uk-margin-small-left" uk-grid="masonry: true">

            <div class="">
                <div class="uk-card uk-card-default uk-card-body">
                    <h1>Welcome</h1>
                </div>
            </div>
            <div class="uk-card uk-card-default ">
                <div class="uk-card-media-top">
                    <img src="images/light.jpg" alt="">
                </div>
                <div class="uk-card-body">
                    <h3 class="uk-card-title"><img src="../Img/Blog.jpg"></h3>
                    <h2>Autre Blog</h2>
                    <p>
                        45 followers,13 Posts
                    </p>
                </div>
            </div>
            <div class="uk-card uk-card-default ">
                <div class="uk-card-media-top">
                    <img src="images/light.jpg" alt="">
                </div>
                <div class="uk-card-body">
                    <h3 class="uk-card-title"><img src="../Img/Blog.jpg"></h3>
                    <h2>Encore un autre Blog</h2>
                    <p>
                        45 followers,13 Posts
                    </p>
                </div>
            </div>
        </div>
    </div>
</body>

</html>