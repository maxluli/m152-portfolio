<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/monCss.css">
    <link rel="stylesheet" href="../css/uikit.min.css" />
    <script src="../js/uikit.min.js"></script>
    <script src="../js/uikit-icons.min.js"></script>
    <script src="https://kit.fontawesome.com/719158633b.js" crossorigin="anonymous"></script>
    <title>Home</title>
</head>

<body class="uk-width-1-1 uk-height-1-1">

    <nav style="background:skyblue" class="uk-navbar-container uk-margin-large-bottom" uk-navbar>

        <div class="uk-navbar-left">

            <ul class="uk-navbar-nav">
                <li class="uk-active"><a href="#">
                        <h1><i class="fas fa-burn uk-margin-top"></h1></i>
                    </a></li>
                <form action="" class="uk-margin-top">
                    <input class="uk-input uk-form-width-small" type="text" placeholder="Input">
                    <button class="uk-button uk-button-default"><i class="fas fa-search"></i></button>
                </form>
            </ul>

        </div>

        <div class="uk-navbar-right">

            <ul class="uk-navbar-nav">
                <li><a href="#">Post &nbsp<i class="fas fa-plus"></i></a></li>
                <li><a href="Home.php">Home &nbsp<i class="fas fa-home"></i></a></li>
            </ul>

        </div>

    </nav>

   
</body>

</html>